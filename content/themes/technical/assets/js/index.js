/*jslint browser: true*/
/*global $, jQuery*/

/**
 * Main JS file for Technical behaviours
 */

/* globals jQuery, document */
(function ($, sr) {
    "use strict";

    var $document = $(document),

        // debouncing function from John Hann
        // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
        debounce = function (func, threshold, execAsap) {
            var timeout;

            return function debounced() {
                var obj = this,
                    args = arguments;

                function delayed() {
                    if (!execAsap) {
                        func.apply(obj, args);
                    }
                    timeout = null;
                }

                if (timeout) {
                    clearTimeout(timeout);
                } else if (execAsap) {
                    func.apply(obj, args);
                }

                timeout = setTimeout(delayed, threshold || 100);
            };
        };

    $document.ready(function () {

        var $postContent = $(".post-content");
        $postContent.fitVids();

        function updateImageWidth() {
            var $this = $(this),
                contentWidth = $postContent.outerWidth(), // Width of the content
                imageWidth = this.naturalWidth; // Original image resolution

            if (imageWidth >= contentWidth) {
                $this.addClass('full-img');
            } else {
                $this.removeClass('full-img');
            }
        }

        var $img = $("img").on('load', updateImageWidth);

        function casperFullImg() {
            $img.each(updateImageWidth);
        }

        casperFullImg();
        $(window).smartresize(casperFullImg);
    });

    // smartresize
    jQuery.fn[sr] = function (fn) {
        return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr);
    };
}(jQuery, 'smartresize'));

/**
 * Parallax effect for the headers.
 */
var isMobile = false,
    header,
    headerContent,
    headerContentOpacity,
    headerPos,
    overlay,
    overlayOpacity,
    lastPosition = -1;

// Detect request animation frame
var scroll = window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    window.msRequestAnimationFrame ||
    window.oRequestAnimationFrame ||
    // IE Fallback, you can even fallback to onscroll
    function (callback) {
        window.setTimeout(callback, (1000 / 60));
    };

// Assign the elements
header = $('header.main-header');
headerContent = $('header .parallax');
overlay = $('.pattern-overlay', header);

function loop() {
    var top = window.pageYOffset,
        innerHeight = window.innerHeight,
        opacityDivisor = 1.75,
        translate = 0;

    isMobile = innerHeight < 500;

    // Avoid calculations if not needed
    if (lastPosition === window.pageYOffset) {
        scroll(loop);
        
        return;
    }

    lastPosition = window.pageYOffset;

    headerPos = Math.min(50 + (top * 0.075), 100) + '%';
    translate = 'translate(0, -' + Math.min((top * 0.1), 75) + 'px)';

    if (isMobile) {
        opacityDivisor = 0.65;
    }

    // Do this calculation once
    opacityModifier = ((top / (innerHeight / opacityDivisor)) * 100);

    headerContentOpacity = (100 - opacityModifier) / 100;

    header.css('background-position-y', headerPos);

    headerContent.css({
        opacity: Math.max(headerContentOpacity, 0),
        transform: translate
    });

    overlayOpacity = (65 + opacityModifier) / 100;
    overlay.css('opacity', Math.min(overlayOpacity, 1));

    // Recall the loop
    scroll(loop);
}

// Call the loop for the first time
loop();
